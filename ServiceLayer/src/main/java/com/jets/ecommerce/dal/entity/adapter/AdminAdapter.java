/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jets.ecommerce.dal.entity.adapter;

import com.jets.ecommerce.dal.entity.Admin;
import com.jets.ecommerce.service.beans.AdminBean;

/**
 *
 * @author ibrahim
 */
public class AdminAdapter {

    public static Admin fromBean(AdminBean bean) {
        if (bean == null) {
            return null;
        }
        Admin admin = new Admin(bean.getName(), bean.getEmail(), bean.getPassword());
        admin.setPhone(bean.getPhone());
        return admin;
    }

    public static AdminBean toBean(Admin entity) {
        if (entity == null) {
            return null;
        }
        AdminBean adminBean = new AdminBean(entity.getId());
        adminBean.setName(entity.getName());
        adminBean.setEmail(entity.getEmail());
        adminBean.setPhone(entity.getPhone());
        return adminBean;
    }
}
