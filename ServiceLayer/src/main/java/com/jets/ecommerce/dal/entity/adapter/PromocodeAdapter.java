/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jets.ecommerce.dal.entity.adapter;

import com.jets.ecommerce.dal.entity.Promocode;
import com.jets.ecommerce.service.beans.PromocodeBean;

/**
 *
 * @author ibrahim
 */
public class PromocodeAdapter {

    public static Promocode fromBean(PromocodeBean bean) {
        if (bean == null) {
            return null;
        }
        Promocode promocode = new Promocode(bean.getCode(), bean.getDiscount());
        return promocode;
    }

    public static PromocodeBean toBean(Promocode entity) {
        if (entity == null) {
            return null;
        }
        PromocodeBean promocodeBean = new PromocodeBean(entity.getId());
        promocodeBean.setCode(entity.getCode());
        promocodeBean.setDiscount(entity.getDiscount());
        return promocodeBean;
    }
}
